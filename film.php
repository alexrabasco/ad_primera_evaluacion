<?php
include_once 'class/Film.php';
require_once 'class/DBSingleton.php';



switch ($_POST['method']) {
    case 'listado':
        $response["paginated"]= true;
        $response["page"] = 1000;
        $response["num_rows"] = 25;
        $response["rows_per_page"] = 10;
        $response["msg"] = "Listado de peliculas";
        $response["succes"] = true;
        $response["data"] = Film::listado($response['num_rows'], $response['page']);
        echo json_encode($response); 
        break;

    case 'borrar':
        $film=new Film($_POST['film_id']);
        $film->borrar();
        $response["succes"] = true;
        $response["msg"] = "Registro Eliminado";
        echo json_encode($response);
        break;

        case 'actualizar':
            $film=new Film($_POST['title'],$_POST['description'], $_POST['release_year'], $_POST['language_id'],
            $_POST['length'] );
            $film->actualizar();
            $response["msg"] = "Registro Actualizado";
            $response["succes"] = true;
            $response["data"] = $film;
            echo json_encode($response); 
            break;

        case 'insert':
            $film=new Film($_POST['film_id'],$_POST['title'],$_POST['description'], $_POST['release_year'], $_POST['language_id'],
            $_POST['length'] );
            $film->insert();
            $response["succes"] = true;
            $response["msg"] = "Registro insertado";
            $response["data"] = $film;
            echo json_encode($response); 
            break;
            
    default:

        break;
}


?>