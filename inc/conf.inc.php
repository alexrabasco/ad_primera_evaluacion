<?php 

try {
  $con = new PDO('mysql:host=localhost;dbname=sakila;charset=utf8','root','');
  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  echo 'Error conectando con la base de datos: ' . $e->getMessage();
}
 ?>