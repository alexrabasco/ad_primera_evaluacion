<?php

//require_once './inc/conf.inc.php';

class DBSingleton{
    // Instanciamos la clase
  private static $instance = null;
  private $conn;
  
  private $host = 'localhost';
  private $user = 'root';
  private $pass = '';
  private $name = 'sakila';
   
  // Conectamos lso parametros a la base de datos
  private function __construct()
  {
    $this->conn = new PDO("mysql:host={$this->host};
    dbname={$this->name}", $this->user,$this->pass,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  }
  
  // Establecemos la instancia
  public static function getInstance()
  {
    if(!self::$instance)
    {
      self::$instance = new DBSingleton();
    }
   
    return self::$instance;
  }
  
  public function getConnection()
  {
    return $this->conn;
  }
}

?>