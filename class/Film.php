<?php 
class Film{

// Parametros de clase Film
var $film_id;
var $title;
var $description;
var $release_year;
var $language_id;
var $length; 


private static $tableName = "film";

// Constructor de la clase

public function __construct($film_id = null, $title = null, $description = null,
$release_year = null, $language_id = null, $length = null){

   $this-> film_id = $film_id;
   $this-> title = $title;
   $this-> description = $description;
   $this-> release_year = $release_year;
   $this-> language_id = $language_id;
   $this-> length = $length;
    
}

// Getters
public function getFilm_id(){
    return $this-> film_id;
}
public function getTitle(){
    return $this-> title;
}
public function getDescription(){
    return $this-> description;
}
public function getRelease_year(){
    return $this-> release_year;
}
public function getLanguage_id(){
    return $this-> language_id;
}
public function getLength(){
    return $this-> length;
}

// Setters
public function setFilm_id($film_id){
    $this-> film_id = $film_id;
}
public function setTitle($title){
    $this-> title = $title;
}
public function setDescription($description){
    $this-> desciption = $description;
}
public function setRelease_id($release_year){
    $this-> release_year = $release_year;
}
public function setLanguage_id($language_id){
    $this-> language_id = $language_id;
}
public function setLength($length){
    $this-> length = $length;
}

// Funcion de insertar
public function insert(){
    $cc = DBSingleton::getInstance();
    $sql = 'INSERT into film (film_id, title, description, release_year, language_id, length) values 
    ("'.$this->getFilm_id().'","'.$this->getTitle().'","'.$this->getDescription().'", "'.$this->getRelease_year().'"
    , "'.$this->getLanguage_id().'",'.$this->getLength().')' ;
    $result = $cc->getConnection()->prepare($sql);
    $result->execute();
}

//Funcion de borrar
public function borrar(){
    $cc = DBSingleton::getInstance();
    $sql = 'DELETE from film where film_id='.$_POST['film_id'] ;
    $result = $cc->getConnection()->prepare($sql);
    $result->execute();
}


// Funcion para listar registros
public static function listado($num_rows, $page ){
    $cc = DBSingleton::getInstance();
    $sql = 'SELECT film_id, title, description, release_year, language_id, length from film limit '. $page.','. $num_rows.'';
    $result = $cc->getConnection()->prepare($sql);
    $result->execute();

    while ($datos = $result->fetch(PDO::FETCH_ASSOC)) {
        $peliculas[]=new Film($datos['film_id'],$datos['title'], $datos['description'], $datos['release_year'], $datos['language_id'], 
        $datos['length']);
    }
    return $peliculas;
}


// Funcion de actualizar
public function actualizar(){
    $cc = DBSingleton::getInstance();
    $sql = 'UPDATE  film  set  title="'.$this->getTitle().'", description="'.$this->getDescription().'", release_year='.$this->getRelease_year() 
    .', language_id='.$this->getLanguage_id().', length='.$this->getLength().' where film_id='.$this->getFilm_id() ;
    $result = $cc->getConnection()->prepare($sql);
    $result->execute();
}


}
?>